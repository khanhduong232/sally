import React from 'react';
import AppRoutes from './src/navigation/appRoutes';

const App = () => {
  return <AppRoutes />;
};
export default App;

import React from 'react';
import StackNavigation from './Stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';

class AppRoutes extends React.PureComponent {
  render() {
    return (
      <NavigationContainer>
        <SafeAreaProvider>
          <StackNavigation />
        </SafeAreaProvider>
      </NavigationContainer>
    );
  }
}
export default AppRoutes;

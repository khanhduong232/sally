/* eslint-disable prettier/prettier */
import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Login/Login';
import Home from '../screens/Home/Home';
import { ROUTES } from '../ultilities/constant';

const {Navigator, Screen} = createStackNavigator();

export default function StackNavigator() {
  return (
    <Navigator headerMode="none">
      <Screen name={ROUTES.login} component={Login} />
      <Screen name={ROUTES.home} component={Home}  />
    </Navigator>
  );
}

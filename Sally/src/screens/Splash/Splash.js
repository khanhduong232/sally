import React from 'react';
import {StyleSheet, ScrollView, View, Image} from 'react-native';

export default class SplashScreen extends React.Component {
  render() {
    return (
      <View style={styles.imageStyle}>
        <Image
          style={styles.imageStyle}
          source={require('../../assets/images/splash.png')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageStyle: {
    flex: 1,
  },
});

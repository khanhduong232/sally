import 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet, ScrollView, Text, View, Image} from 'react-native';
import {getWidth, getHeight} from '../../components/getSize';

class Home extends React.Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.headerContainer}>
          <Image
            style={styles.ellipseStyle}
            source={require('../../assets/images/Ellipse.png')}
          />
          <Text style={styles.textStyle}>Hi Sophisticateddev</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image source={require('../../assets/images/Rectangle_1.png')} />
          <View style={styles.imageRow}>
            <Image source={require('../../assets/images/Rectangle_2.png')} />
            <Image source={require('../../assets/images/Rectangle_2.png')} />
          </View>
          <View style={styles.imageColumn}>
            <Image
              style={{marginBottom: getHeight(20)}}
              source={require('../../assets/images/Rectangle_3.png')}
            />
            <Image source={require('../../assets/images/Rectangle_3.png')} />
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    flexDirection: 'row',
  },
  ellipseStyle: {
    marginLeft: getWidth(20),
    marginTop: getHeight(21),
    marginRight: getWidth(12),
    marginBottom: getHeight(29),
  },
  textStyle: {
    fontSize: 24,
    color: '#333333',
    fontWeight: '600',
    marginTop: getHeight(29),
  },
  imageContainer: {
    marginLeft: getWidth(20),
    marginRight: getWidth(21),
  },
  imageRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: getHeight(20),
  },
  imageColumn: {
    flexDirection: 'column',
    marginVertical: getHeight(20),
    justifyContent: 'space-between',
  },
});

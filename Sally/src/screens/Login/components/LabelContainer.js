import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {getWidth, getHeight} from '../../../components/getSize';

export default function LabelContainer() {
  return (
    <View>
      <Image
        style={styles.logoChar}
        source={require('../../../assets/images/sally-cha.png')}
      />
      <Image
        style={styles.logoName}
        source={require('../../../assets/images/Sally.png')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  logoChar: {
    marginTop: getHeight(69),
    marginHorizontal: getWidth(65),
  },
  logoName: {
    marginHorizontal: getWidth(171),
    marginTop: getHeight(46),
  },
});

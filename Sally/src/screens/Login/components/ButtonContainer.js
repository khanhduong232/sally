// import React from 'react';
// import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
// import {getWidth, getHeight} from '../../../components/getSize';
// import {ROUTES} from '../../../ultilities/constant';

// export default class ButtonContainer extends React.Component {
//   moveToHome = () => {
//     this.props.navigation.navigate(ROUTES.home);
//   };
//   render() {
//     return (
//       <View>
//         <TouchableOpacity style={styles.buttonLogin} onPress={this.moveToHome}>
//           <Text style={styles.text}>Login</Text>
//         </TouchableOpacity>
//         <View style={styles.textContainer}>
//           <Text style={styles.text}>New to sally? </Text>
//           <TouchableOpacity>
//             <Text style={styles.textTouch}>Sign up</Text>
//           </TouchableOpacity>
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   buttonLogin: {
//     backgroundColor: '#1E57F1',
//     borderColor: '#1E57F1',
//     alignItems: 'center',
//     paddingVertical: getHeight(21),
//     marginBottom: getHeight(28),
//     marginHorizontal: getWidth(20),
//   },
//   textContainer: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   text: {
//     fontSize: 18,
//   },
//   textTouch: {
//     fontSize: 18,
//     color: '#1E57F1',
//   },
// });

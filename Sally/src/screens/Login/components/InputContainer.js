import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  Text,
  Alert,
} from 'react-native';
import {getHeight, getWidth} from '../../../components/getSize';
import {ROUTES} from '../../../ultilities/constant';

export default class InputContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      isFocusedEmail: false,
      isFocusedPass: false,
      textInputEmail: '',
      textInputPassword: '',
    };
    this.showHidePassword = this.showHidePassword.bind(this);
  }

  handleFocusEmail = () => this.setState({isFocusedEmail: true});
  handleBlurEmail = () => {
    this.setState({isFocusedEmail: false});
    let regEmail =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regEmail.test(this.state.textInputEmail)) {
      Alert.alert('Invalid email');
    }
  };

  handleFocusPass = () => this.setState({isFocusedPass: true});
  handleBlurPass = () => this.setState({isFocusedPass: false});

  changeTextEmail = textInputEmail => {
    this.setState({textInputEmail: textInputEmail});
  };
  changeTextPass = text => {
    this.setState({textInputPassword: text});
  };

  showHidePassword = () => {
    this.setState(prev => ({hidePassword: !prev.hidePassword}));
  };

  checkTextInput = () => {
    if (!this.state.textInputEmail.trim()) {
      Alert.alert('Please Enter Name');
    } else if (!this.state.textInputPassword.trim()) {
      Alert.alert('Please Enter Password');
    } else {
      this.props.navigation.navigate(ROUTES.home);
    }
  };

  render() {
    console.log('email: ', this.state.textInputEmail);
    console.log('pass: ', this.state.textInputPassword);
    return (
      <>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Email"
            onFocus={this.handleFocusEmail}
            onBlur={this.handleBlurEmail}
            style={
              this.state.isFocusedEmail
                ? styles.emailInputFocused
                : styles.emailInput
            }
            // style={styles.emailInput}
            onChangeText={this.changeTextEmail}
          />

          <View
            style={
              this.state.isFocusedPass
                ? styles.passwordContainerFocused
                : styles.passwordContainer
            }>
            <TextInput
              placeholder="Password"
              onFocus={this.handleFocusPass}
              onBlur={this.handleBlurPass}
              style={styles.passwordInput}
              secureTextEntry={this.state.hidePassword}
              onChangeText={this.changeTextPass}
            />
            <TouchableOpacity
              style={styles.imageContainer}
              onPress={this.showHidePassword}>
              <Image
                source={
                  this.state.hidePassword
                    ? require('../../../assets/images/eye-off.png')
                    : require('../../../assets/images/eye-on.png')
                }
              />
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <TouchableOpacity
            style={styles.buttonLogin}
            onPress={this.checkTextInput}>
            <Text style={styles.text}>Login</Text>
          </TouchableOpacity>
          <View style={styles.textContainer}>
            <Text style={styles.text}>New to sally? </Text>
            <TouchableOpacity>
              <Text style={styles.textTouch}>Sign up</Text>
            </TouchableOpacity>
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    paddingTop: getHeight(32),
    paddingHorizontal: getWidth(20),
  },
  emailInput: {
    borderColor: '#C7C7C7',
    borderWidth: 1,
    fontSize: 18,
    fontWeight: '400',
    marginBottom: getHeight(22),
    paddingVertical: getHeight(21),
    paddingLeft: getWidth(22),
    flex: 1,
  },
  emailInputFocused: {
    borderColor: '#1E57F1',
    borderWidth: 1,
    fontSize: 18,
    fontWeight: '400',
    marginBottom: getHeight(22),
    paddingVertical: getHeight(21),
    paddingLeft: getWidth(22),
    flex: 1,
  },
  passwordInput: {
    //borderColor: '#C7C7C7',
    fontSize: 18,
    fontWeight: '400',
    paddingVertical: getHeight(21),
    paddingLeft: getWidth(22),
    flex: 1,
  },
  passwordContainerFocused: {
    borderColor: '#1E57F1',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    marginBottom: getHeight(26),
  },
  passwordContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#C7C7C7',
    marginBottom: getHeight(26),
  },
  imageContainer: {
    paddingRight: getWidth(19),
    paddingVertical: getHeight(19),
  },
  buttonLogin: {
    backgroundColor: '#1E57F1',
    borderColor: '#1E57F1',
    alignItems: 'center',
    paddingVertical: getHeight(21),
    marginBottom: getHeight(28),
    marginHorizontal: getWidth(20),
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
  },
  textTouch: {
    fontSize: 18,
    color: '#1E57F1',
  },
});

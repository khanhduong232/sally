import React from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import LabelContainer from './components/LabelContainer';
import InputContainer from './components/InputContainer';
import ButtonContainer from './components/ButtonContainer';
import SplashScreen from '../Splash/Splash';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isLoading: true};
  }
  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };
  async componentDidMount() {
    const data = await this.performTimeConsumingTask();
    if (data !== null) {
      this.setState({isLoading: false});
    }
  }
  render() {
    if (this.state.isLoading) {
      return <SplashScreen />;
    }
    return (
      <ScrollView style={styles.container}>
        <LabelContainer />
        <InputContainer navigation={this.props.navigation} />
        {/* <ButtonContainer navigation={this.props.navigation} /> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});

import {Dimensions} from 'react-native';

const DESIGN = {
  WIDTH: 414,
  HEIGHT: 896,
};
export const {width: DEVICE_WIDTH, height: DEVICE_HEIGHT} =
  Dimensions.get('window');

export const getWidth = size => {
  return (DEVICE_WIDTH * size) / DESIGN.WIDTH;
};

export const getHeight = size => {
  return (DEVICE_HEIGHT * size) / DESIGN.HEIGHT;
};
